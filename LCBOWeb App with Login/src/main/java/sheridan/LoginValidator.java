package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		if(loginName.length() < 6) {
			return false;
		}else {
			for(int i =0; i <loginName.length(); i++) {
				if((Character.isLetterOrDigit(loginName.charAt(i)) == false)) {
					return false;
				}
			}
			return true;
		}
	}
}
