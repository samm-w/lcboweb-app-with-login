package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginAlphaNumericCharactersRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses6518" ) );
	}
	
	@Test
	public void testIsValidLoginAlphaNumericCharactersException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "*ram%ses" ) );
	}
	
	@Test
	public void testIsValidLoginAlphaNumericCharactersBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramse5" ) );
	}
	
	@Test
	public void testIsValidLoginAlphaNumericCharactersBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "r*" ) );
	}
	
	@Test
	public void testIsValidLoginValidLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses1953" ) );
	}
	
	@Test
	public void testIsValidLoginValidLengthException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "ram" ) );
	}
	
	@Test
	public void testIsValidLoginValidLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginValidLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "ramse" ) );
	}

}
